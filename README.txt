
Module: REST Entity Index
Author: Malabya Tewari <https://drupal.org/user/2641255>


Description
===========
REST Entity Index provides an index API to list entities.

Requirements
============

This module requires the following modules:


Installation
============
Copy the 'rest_entity_index' module directory in to your Drupal 'modules'
directory as usual.



Configuration
=============



MAINTAINERS
===========

Current maintainers:
  
  * Malabya Tewari <https://drupal.org/user/2641255>
